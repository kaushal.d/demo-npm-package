## Installation

```sh
$ npm install first45_package
```

## API

<!-- eslint-disable no-unused-vars -->

```js
var print = require('first45_package')
```

### .printMsg()

Prints message `This is a message from the demo package`.
